import {
    FETCH_ROOMWITHTYPE_STARTED,
    FETCH_ROOMWITHTYPE_SUCCESS,
    FETCH_ROOMWITHTYPE_FAILURE,

    FETCH_ROOMWITHTYPE_LIST_STARTED,
    FETCH_ROOMWITHTYPE_LIST_SUCCESS,
    FETCH_ROOMWITHTYPE_LIST_FAILURE,

    DELETE_ROOMWITHTYPE_STARTED,
    DELETE_ROOMWITHTYPE_SUCCESS,
    DELETE_ROOMWITHTYPE_FAILURE,

    UPDATE_ROOMWITHTYPE_STARTED,
    UPDATE_ROOMWITHTYPE_SUCCESS,
    UPDATE_ROOMWITHTYPE_FAILURE,

    CREATE_ROOMWITHTYPE_STARTED,
    CREATE_ROOMWITHTYPE_SUCCESS,
    CREATE_ROOMWITHTYPE_FAILURE
} from "../actions/actionsRoomsWithType";

const initState = {
    roomWithType: {
        loading: false,
        error: null,
        data: [],
    }

}

export default function roomWithTypeReducer(state = initState, action) {
    switch (action.type) {

        ///// GET ROOM WITH TYPE //////
        case FETCH_ROOMWITHTYPE_STARTED:
            return {
                ...state,
                roomWithType: {
                    loading: true,
                    error: null,
                    data: [],
                    //type: action.payload.type,
                }
            }
        case FETCH_ROOMWITHTYPE_SUCCESS:
            return {
                ...state,
                roomWithType: {
                    loading: false,
                    error: null,
                    data: [...action.payload.data],
                    //type: action.payload.type,
                }
            }
        case FETCH_ROOMWITHTYPE_FAILURE:
            return {
                ...state,
                roomWithType: {
                    loading: false,
                    error: action.payload.error,
                    data: [],
                    //type: '',
                }
            }

        ///// GET LIST ROOM WITH TYPE //////
        case FETCH_ROOMWITHTYPE_LIST_STARTED:
            return {
                ...state,
                roomWithType: {
                    loading: true,
                    error: null,
                    data: [],
                    //type: '',
                }
            }
        case FETCH_ROOMWITHTYPE_LIST_SUCCESS:
            return {
                ...state,
                roomWithType: {
                    loading: false,
                    error: null,
                    data: [...action.payload.data],
                    //type: '',
                }
            }
        case FETCH_ROOMWITHTYPE_LIST_FAILURE:
            return {
                ...state,
                roomWithType: {
                    loading: false,
                    error: action.payload.error,
                    data: [],
                    //type: '',
                }
            }

        ///// DELETE ROOM WITH TYPE //////
        case DELETE_ROOMWITHTYPE_STARTED:
            return {
                ...state,
                roomWithType: {
                    loading: true,
                    error: null,
                    data: [],
                    //type: action.payload.type,
                }
            }
        case DELETE_ROOMWITHTYPE_SUCCESS:
            return {
                ...state,
                roomWithType: {
                    loading: false,
                    error: null,
                    data: state.roomWithType.data.concat(action.payload.data), // <-- ??????????
                    //data: state.sensors.data.filter( row =>  row.idOfSensor !== action.payload.data) // <-- ??????????
                    //type: action.payload.type,
                }
            }
        case DELETE_ROOMWITHTYPE_FAILURE:
            return {
                ...state,
                roomWithType: {
                    loading: false,
                    error: action.payload.error,
                    data: [],
                    //type:''
                }
            }

        ///// UPDATE ROOM WITH TYPE //////
        case UPDATE_ROOMWITHTYPE_STARTED:
            return {
                ...state,
                roomWithType: {
                    loading: true,
                    error: null,
                    data: [],

                }
            }
        case UPDATE_ROOMWITHTYPE_SUCCESS:
            return {
                ...state,
                roomWithType: {
                    loading: false,
                    error: null,
                    data: state.roomWithType.data.concat(action.payload.data),

                }
            }
        case UPDATE_ROOMWITHTYPE_FAILURE:
            return {
                ...state,
                roomWithType: {
                    loading: false,
                    error: action.payload.error,
                    data: [],

                }
            }

        ///// CREATE ROOM WITH TYPE //////
        case CREATE_ROOMWITHTYPE_STARTED:
            return {
                ...state,
                roomWithType: {
                    loading: true,
                    error: null,
                    data: [],

                }
            }
        case CREATE_ROOMWITHTYPE_SUCCESS:
            return {
                ...state,
                roomWithType: {
                    loading: false,
                    error: null,
                    data: state.roomWithType.data.concat(action.payload.data),

                }
            }
        case CREATE_ROOMWITHTYPE_FAILURE:
            return {
                ...state,
                roomWithType: {
                    loading: false,
                    error: action.payload.error,
                    data: [],

                }
            }

        default:
            return state
    }

}

