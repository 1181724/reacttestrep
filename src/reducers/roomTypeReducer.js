import {
FETCH_ROOM_TYPE_STARTED,
FETCH_ROOM_TYPE_SUCCESS,
FETCH_ROOM_TYPE_FAILURE,

FETCH_ROOMS_TYPE_STARTED,
FETCH_ROOMS_TYPE_SUCCESS,
FETCH_ROOMS_TYPE_FAILURE,

DELETE_ROOM_TYPE_STARTED,
DELETE_ROOM_TYPE_SUCCESS,
DELETE_ROOM_TYPE_FAILURE,

UPDATE_ROOM_TYPE_STARTED,
UPDATE_ROOM_TYPE_SUCCESS,
UPDATE_ROOM_TYPE_FAILURE,

CREATE_ROOM_TYPE_STARTED,
CREATE_ROOM_TYPE_SUCCESS,
CREATE_ROOM_TYPE_FAILURE
} from "../actions/actionsRoomType";

const initState = {
    roomType: {
        loading: false,
        error: null,
        data: [],
        //type:''
    }

}

export default function roomTypeReducer(state = initState, action) {
    switch (action.type) {

        ///// GET ROOM TYPE //////
        case FETCH_ROOM_TYPE_STARTED:
            return {
                ...state,
                roomType: {
                    loading: true,
                    error: null,
                    data: [],
                    //type: action.payload.type,
                }
            }
        case FETCH_ROOM_TYPE_SUCCESS:
            return {
                ...state,
                roomType: {
                    loading: false,
                    error: null,
                    data: [...action.payload.data],
                    //type: action.payload.type,
                }
            }
        case FETCH_ROOM_TYPE_FAILURE:
            return {
                ...state,
                roomType: {
                    loading: false,
                    error: action.payload.error,
                    data: [],
                    //type: '',
                }
            }

        ///// GET LIST ROOM TYPE //////
        case FETCH_ROOMS_TYPE_STARTED:
            return {
                ...state,
                roomType: {
                    loading: true,
                    error: null,
                    data: [],
                    //type: '',
                }
            }
        case FETCH_ROOMS_TYPE_SUCCESS:
            return {
                ...state,
                roomType: {
                    loading: false,
                    error: null,
                    data: [...action.payload.data],
                    //type: '',
                }
            }
        case FETCH_ROOMS_TYPE_FAILURE:
            return {
                ...state,
                roomType: {
                    loading: false,
                    error: action.payload.error,
                    data: [],
                    //type: '',
                }
            }

        ///// DELETE ROOM TYPE //////
        case DELETE_ROOM_TYPE_STARTED:
            return {
                ...state,
                roomType: {
                    loading: true,
                    error: null,
                    data: [],
                    //type: action.payload.type,
                }
            }
        case DELETE_ROOM_TYPE_SUCCESS:
            return {
                ...state,
                roomType: {
                    loading: false,
                    error: null,
                    data: state.roomType.data.concat(action.payload.data), // <-- ??????????
                    //data: state.sensors.data.filter( row =>  row.idOfSensor !== action.payload.data) // <-- ??????????
                    //type: action.payload.type,
                }
            }
        case DELETE_ROOM_TYPE_FAILURE:
            return {
                ...state,
                roomType: {
                    loading: false,
                    error: action.payload.error,
                    data: [],
                    //type:''
                }
            }

        ///// UPDATE ROOM TYPE //////
        case UPDATE_ROOM_TYPE_STARTED:
            return {
                ...state,
                roomType: {
                    loading: true,
                    error: null,
                    data: [],

                }
            }
        case UPDATE_ROOM_TYPE_SUCCESS:
            return {
                ...state,
                roomType: {
                    loading: false,
                    error: null,
                    data: state.roomType.data.concat(action.payload.data),

                }
            }
        case UPDATE_ROOM_TYPE_FAILURE:
            return {
                ...state,
                roomType: {
                    loading: false,
                    error: action.payload.error,
                    data: [],

                }
            }

        ///// CREATE ROOM TYPE //////
        case CREATE_ROOM_TYPE_STARTED:
            return {
                ...state,
                roomType: {
                    loading: true,
                    error: null,
                    data: [],

                }
            }
        case CREATE_ROOM_TYPE_SUCCESS:
            return {
                ...state,
                roomType: {
                    loading: false,
                    error: null,
                    data: state.roomType.data.concat(action.payload.data),

                }
            }
        case CREATE_ROOM_TYPE_FAILURE:
            return {
                ...state,
                roomType: {
                    loading: false,
                    error: action.payload.error,
                    data: [],

                }
            }

        default:
            return state
    }

}

