import axios from 'axios'

////// GET ROOM TYPE //////
export const FETCH_ROOM_TYPE_STARTED = 'FETCH_ROOM_TYPE_STARTED'
export const FETCH_ROOM_TYPE_SUCCESS = 'FETCH_ROOM_TYPE_SUCCESS'
export const FETCH_ROOM_TYPE_FAILURE = 'FETCH_ROOM_TYPE_FAILURE'

////// GET LIST ROOM TYPE //////
export const FETCH_ROOMS_TYPE_STARTED = 'FETCH_ROOMS_TYPE_STARTED'
export const FETCH_ROOMS_TYPE_SUCCESS = 'FETCH_ROOMS_TYPE_SUCCESS'
export const FETCH_ROOMS_TYPE_FAILURE = 'FETCH_ROOMS_TYPE_FAILURE'

////// DELETE ROOM TYPE //////
export const DELETE_ROOM_TYPE_STARTED = 'DELETE_ROOM_TYPE_STARTED'
export const DELETE_ROOM_TYPE_SUCCESS = 'DELETE_ROOM_TYPE_SUCCESS'
export const DELETE_ROOM_TYPE_FAILURE = 'DELETE_ROOM_TYPE_FAILURE'

////// UPDATE or PUT or EDIT ROOM TYPE //////
export const UPDATE_ROOM_TYPE_STARTED = 'UPDATE_ROOM_TYPE_STARTED'
export const UPDATE_ROOM_TYPE_SUCCESS = 'UPDATE_ROOM_TYPE_SUCCESS'
export const UPDATE_ROOM_TYPE_FAILURE = 'UPDATE_ROOM_TYPE_FAILURE'

////// CREATE or POST ROOM TYPE //////
export const CREATE_ROOM_TYPE_STARTED = 'CREATE_ROOM_TYPE_STARTED'
export const CREATE_ROOM_TYPE_SUCCESS = 'CREATE_ROOM_TYPE_SUCCESS'
export const CREATE_ROOM_TYPE_FAILURE = 'CREATE_ROOM_TYPE_FAILURE'

////// GET ROOM TYPE //////
export const fetchRoomType = (type) => {
    return dispatch => {
        const token = localStorage.getItem('id_token');

        dispatch(fetchRoomTypeStarted(type));
        axios.get("http://localhost:8443/room-configuration/rest-get/"+type,
            {
                'headers': {
                    'Authorization': token,
                    "Access-Control-Allow-Credentials": true,
                    "Access-Control-Allow-Origin": "*",
                    'Content-Type': 'application/json',
                }
            }
        ).then(res => {
            console.log("SUC");
            dispatch(fetchRoomTypeSuccess(res.data));
        })
            .catch(err => {
                console.log("ERR");
                dispatch(fetchRoomTypeFailure(err.message));
            });
    };
};

export function fetchRoomTypeStarted() {
    return {
        type: FETCH_ROOM_TYPE_STARTED,
        /*payload:{
            data: type
        }*/
    }
}

export function fetchRoomTypeSuccess(roomType) {
    return {
        type: FETCH_ROOM_TYPE_SUCCESS,
        payload: {
            data:
                [...roomType]
        }

    }
}

export function fetchRoomTypeFailure(message) {
    return {
        type: FETCH_ROOM_TYPE_FAILURE,
        payload: {
            error: [...message]
        }
    }
}

////// GET LIST ROOM TYPE//////
export const fetchRoomsType = () => {
    return dispatch => {
        const token = localStorage.getItem('id_token');

        dispatch(fetchRoomsTypeStarted());
        axios.get("http://localhost:8443/room-configuration/rooms-types-rest",
            {
                'headers': {
                    'Authorization': token,
                    "Access-Control-Allow-Credentials": true,
                    "Access-Control-Allow-Origin": "*",
                    'Content-Type': 'application/json',
                }
            }
        ).then(res => {
            dispatch(fetchRoomsTypeSuccess(res.data));
        })
            .catch(err => {
                dispatch(fetchRoomsTypeFailure(err.error));
            });
    };
};

export function fetchRoomsTypeStarted() {
    return {
        type: FETCH_ROOMS_TYPE_STARTED,
    }
}

export function fetchRoomsTypeSuccess(roomsType) {
    return {
        type: FETCH_ROOMS_TYPE_SUCCESS,
        payload: {
            data:
                [...roomsType]
        }

    }
}

export function fetchRoomsTypeFailure(message) {
    return {
        type: FETCH_ROOMS_TYPE_FAILURE,
        payload: {
            error: message
        }
    }
}

////// DELETE ROOM TYPE //////
export const deleteRoomsType = (type) => {
    return dispatch => {
        const token = localStorage.getItem('id_token');

        dispatch(deleteRoomTypeStarted(type));
        axios.delete("http://localhost:8443/room-configuration/rest-delete/"+type,
            {
                'headers': {
                    'Authorization': token,
                    "Access-Control-Allow-Credentials": true,
                    "Access-Control-Allow-Origin": "*",
                    'Content-Type': 'application/json',
                }
            }
        ).then(res => {
            dispatch(deleteRoomTypeSuccess(res.response));
            //window.confirm("Room Type Deleted...")
        })
            //if(data.type === undefined){
                .catch(err => {
                    dispatch(deleteRoomTypeFailure(err.message));
                });
            //}
    };
};

export function deleteRoomTypeStarted(type) {
    return {
        type: DELETE_ROOM_TYPE_STARTED,
        payload: type
    }
}

export function deleteRoomTypeSuccess(response) {
    return {
        type: DELETE_ROOM_TYPE_SUCCESS,
        payload: response
    }
}

export function deleteRoomTypeFailure(message) {
    return {
        type: DELETE_ROOM_TYPE_FAILURE,
        payload: {
            error: message
        }
    }
}

////// UPDATE or PUT or EDIT ROOM TYPE //////
export const editRoomType = ({type, minHeight, minWidth, minLength}) => {
    return dispatch => {
        const token = localStorage.getItem('id_token');

        dispatch(editRoomTypeStarted(type, minHeight, minWidth, minLength));
        axios.put("http://localhost:8443/room-configuration/rest-update/" + type,
            {type:type,minHeight:minHeight,minWidth:minWidth,minLength:minLength},
            {
                'headers': {
                    'Authorization': token,
                    "Access-Control-Allow-Credentials": true,
                    "Access-Control-Allow-Origin": "*",
                    'Content-Type': 'application/json',
                }
            }
        ).then(res => {
            dispatch(editRoomTypeSuccess(res.data));
        })
            .catch(err => {
                dispatch(editRoomTypeFailure(err.message));
            });
    };
};

export function editRoomTypeStarted() {
    return {
        type: UPDATE_ROOM_TYPE_STARTED,
    }
}

export function editRoomTypeSuccess(RoomType) {
    return {
        type: UPDATE_ROOM_TYPE_SUCCESS,
        payload: {
            data: RoomType
        }
    }
}

export function editRoomTypeFailure(message) {
    return {
        type: UPDATE_ROOM_TYPE_FAILURE,
        payload: {
            error: message
        }
    }
}

////// CREATE or POST ROOM TYPE //////
export const createRoomType = ({type, minHeight, minWidth, minLength}) => {
    return dispatch => {
        const token = localStorage.getItem('id_token');
        dispatch(createRoomTypeStarted(type, minHeight, minWidth, minLength));

        axios.post("http://localhost:8443/room-configuration/rest-new/",
            {type:type,minHeight:minHeight,minWidth:minWidth, minLength:minLength},
            {
                headers: {
                    'Authorization': token,
                    "Access-Control-Allow-Credentials": true,
                    "Access-Control-Allow-Origin": "*",
                    'Content-Type': 'application/json',
                },
            }
        ).then(res => {
            dispatch(createRoomTypeSuccess(res.data));
        })
            .catch(error => {
                //error.then((e) => {dispatch(createRoomTypeFailure(error.message))})

                dispatch(createRoomTypeFailure(error.message));
                //alert("error");
                console.log('log_dis:' + error.message)
            });
    };
};

export function createRoomTypeStarted(type, minHeight, minWidth, minLength) {
    return {
        type: CREATE_ROOM_TYPE_STARTED,
        payload: {type:type, minHeight:minHeight, minWidth:minWidth, minLength:minLength}

    }
}

export function createRoomTypeSuccess(data) {
    return {
        type: CREATE_ROOM_TYPE_SUCCESS,
        payload: {
            data:data
        }

    }
}

export function createRoomTypeFailure(message) {
    console.log('log_act:' + message)
    return {
        type: CREATE_ROOM_TYPE_FAILURE,
        payload: {
            error: message
        }
    }
}