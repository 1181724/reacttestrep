import axios from 'axios'

////// GET ROOM TYPE //////
export const FETCH_ROOMWITHTYPE_STARTED = 'FETCH_ROOMWITHTYPE_STARTED'
export const FETCH_ROOMWITHTYPE_SUCCESS = 'FETCH_ROOMWITHTYPE_SUCCESS'
export const FETCH_ROOMWITHTYPE_FAILURE = 'FETCH_ROOMWITHTYPE_FAILURE'

////// GET LIST ROOM WITH TYPE //////
export const FETCH_ROOMWITHTYPE_LIST_STARTED = 'FETCH_ROOMWITHTYPE_LIST_STARTED'
export const FETCH_ROOMWITHTYPE_LIST_SUCCESS = 'FETCH_ROOMWITHTYPE_LIST_SUCCESS'
export const FETCH_ROOMWITHTYPE_LIST_FAILURE = 'FETCH_ROOMWITHTYPE_LIST_FAILURE'

////// DELETE ROOM WITH TYPE //////
export const DELETE_ROOMWITHTYPE_STARTED = 'DELETE_ROOMWITHTYPE_STARTED'
export const DELETE_ROOMWITHTYPE_SUCCESS = 'DELETE_ROOMWITHTYPE_SUCCESS'
export const DELETE_ROOMWITHTYPE_FAILURE = 'DELETE_ROOMWITHTYPE_FAILURE'

////// UPDATE or PUT or EDIT ROOM WITH TYPE //////
export const UPDATE_ROOMWITHTYPE_STARTED = 'UPDATE_ROOMWITHTYPE_STARTED'
export const UPDATE_ROOMWITHTYPE_SUCCESS = 'UPDATE_ROOMWITHTYPE_SUCCESS'
export const UPDATE_ROOMWITHTYPE_FAILURE = 'UPDATE_ROOMWITHTYPE_FAILURE'

////// CREATE or POST ROOM WITH TYPE //////
export const CREATE_ROOMWITHTYPE_STARTED = 'CREATE_ROOMWITHTYPE_STARTED'
export const CREATE_ROOMWITHTYPE_SUCCESS = 'CREATE_ROOMWITHTYPE_SUCCESS'
export const CREATE_ROOMWITHTYPE_FAILURE = 'CREATE_ROOMWITHTYPE_FAILURE'


////// GET ROOM WITH TYPE //////
export const fetchRoomWithType = (type) => {
    return dispatch => {
        const token = localStorage.getItem('id_token');

        dispatch(fetchRoomWithTypeStarted(type));
        axios.get("http://localhost:8443/room-configuration/rest-get/"+type,
            {
                'headers': {
                    'Authorization': token,
                    "Access-Control-Allow-Credentials": true,
                    "Access-Control-Allow-Origin": "*",
                    'Content-Type': 'application/json',
                }
            }
        ).then(res => {
            console.log("SUC");
            dispatch(fetchRoomWithTypeSuccess(res.data));
        })
            .catch(err => {
                console.log("ERR");
                dispatch(fetchRoomWithTypeFailure(err.message));
            });
    };
};

export function fetchRoomWithTypeStarted() {
    return {
        type: FETCH_ROOMWITHTYPE_STARTED,
        /*payload:{
            data: type
        }*/
    }
}

export function fetchRoomWithTypeSuccess(roomType) {
    return {
        type: FETCH_ROOMWITHTYPE_SUCCESS,
        payload: {
            data:
                [...roomType]
        }

    }
}

export function fetchRoomWithTypeFailure(message) {
    return {
        type: FETCH_ROOMWITHTYPE_FAILURE,
        payload: {
            error: [...message]
        }
    }
}

////// GET ROOM WITH TYPE LIST ///////
export const fetchRoomsWithType = () => {
    return dispatch => {
        dispatch(fetchRoomsWithTypeStarted());
        const token = localStorage.getItem('id_token');
        axios.get("http://localhost:8443/room-configuration/rest/list-of-rooms",
            {
                'headers': {
                    'Authorization': token,
                    "Access-Control-Allow-Credentials": true,
                    "Access-Control-Allow-Origin": "*",
                    'Content-Type': 'application/json',
                }
            }
        ).then(res => {
            dispatch(fetchRoomsWithTypeSuccess(res.data));
        })
            .catch(err => {
                dispatch(fetchRoomsWithTypeFailure(err.response.data));
            });
    };
};

export function fetchRoomsWithTypeStarted() {
    return {
        type: FETCH_ROOMWITHTYPE_LIST_STARTED,
    }
}

export function fetchRoomsWithTypeSuccess(roomWithType) {
    return {
        type: FETCH_ROOMWITHTYPE_LIST_SUCCESS,
        payload: {
            data:
                [...roomWithType]
        }
    }
}

export function fetchRoomsWithTypeFailure(message) {
    return {
        type: FETCH_ROOMWITHTYPE_LIST_FAILURE,
        payload: {
            error: message
        }
    }
}

////// DELETE ROOM WITH TYPE //////
export const deleteRoomsWithType = (type) => {
    return dispatch => {
        const token = localStorage.getItem('id_token');

        dispatch(deleteRoomWithTypeStarted(type));
        axios.delete("http://localhost:8443/room-configuration/rest-delete/"+type,
            {
                'headers': {
                    'Authorization': token,
                    "Access-Control-Allow-Credentials": true,
                    "Access-Control-Allow-Origin": "*",
                    'Content-Type': 'application/json',
                }
            }
        ).then(res => {
            dispatch(deleteRoomWithTypeSuccess(res.response));
            //window.confirm("Room Type Deleted...")
        })
        //if(data.type === undefined){
            .catch(err => {
                dispatch(deleteRoomWithTypeFailure(err.message));
            });
        //}
    };
};

export function deleteRoomWithTypeStarted(type) {
    return {
        type: DELETE_ROOMWITHTYPE_STARTED,
        payload: type
    }
}

export function deleteRoomWithTypeSuccess(response) {
    return {
        type: DELETE_ROOMWITHTYPE_SUCCESS,
        payload: response
    }
}

export function deleteRoomWithTypeFailure(message) {
    return {
        type: DELETE_ROOMWITHTYPE_FAILURE,
        payload: {
            error: message
        }
    }
}

////// UPDATE or PUT or EDIT ROOM TYPE //////
export const editRoomWithType = ({type, minHeight, minWidth, minLength}) => {
    return dispatch => {
        const token = localStorage.getItem('id_token');

        dispatch(editRoomWithTypeStarted(type, minHeight, minWidth, minLength));
        axios.put("http://localhost:8443/room-configuration/rest-update/" + type,
            {type:type,minHeight:minHeight,minWidth:minWidth,minLength:minLength},
            {
                'headers': {
                    'Authorization': token,
                    "Access-Control-Allow-Credentials": true,
                    "Access-Control-Allow-Origin": "*",
                    'Content-Type': 'application/json',
                }
            }
        ).then(res => {
            dispatch(editRoomWithTypeSuccess(res.data));
        })
            .catch(err => {
                dispatch(editRoomWithTypeFailure(err.message));
            });
    };
};

export function editRoomWithTypeStarted() {
    return {
        type: UPDATE_ROOMWITHTYPE_STARTED,
    }
}

export function editRoomWithTypeSuccess(RoomType) {
    return {
        type: UPDATE_ROOMWITHTYPE_SUCCESS,
        payload: {
            data: RoomType
        }
    }
}

export function editRoomWithTypeFailure(message) {
    return {
        type: UPDATE_ROOMWITHTYPE_FAILURE,
        payload: {
            error: message
        }
    }
}

////// CREATE or POST ROOM TYPE //////
export const createRoomWithType = ({name, roomType, description, houseFloor, height, width, length}) => {
    return dispatch => {
        const token = localStorage.getItem('id_token');
        dispatch(createRoomWithTypeStarted(name, roomType, description, houseFloor, height, width, length));

        axios.post("http://localhost:8443/room-configuration/rest/rest-new",
            {name:name,
                roomType:roomType,
                description:description,
                houseFloor:houseFloor,
                height:height,width:width,length:length},
            {
                headers: {
                    'Authorization': token,
                    "Access-Control-Allow-Credentials": true,
                    "Access-Control-Allow-Origin": "*",
                    'Content-Type': 'application/json',
                },
            }
        ).then(res => {
            dispatch(createRoomWithTypeSuccess(res.data));
        })
            .catch(error => {
                //error.then((e) => {dispatch(createRoomTypeFailure(error.message))})

                dispatch(createRoomWithTypeFailure(error.message));
                //alert("error");
                console.log('log_dis:' + error.message)
            });
    };
};

export function createRoomWithTypeStarted(name, roomType, description, houseFloor, height, width, length) {
    return {
        type: CREATE_ROOMWITHTYPE_STARTED,
        payload: {name:name,
            roomType:roomType,
            description:description,
            houseFloor:houseFloor,
            height:height,width:width,length:length}
    }
}

export function createRoomWithTypeSuccess(data) {
    return {
        type: CREATE_ROOMWITHTYPE_SUCCESS,
        payload: {
            data:data
        }
    }
}

export function createRoomWithTypeFailure(message) {
    console.log('log_act:' + message)
    return {
        type: CREATE_ROOMWITHTYPE_FAILURE,
        payload: {
            error: message
        }
    }
}