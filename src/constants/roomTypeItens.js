const roomtype = [
    {
        type: "bedroom",
        minLength: 3,
        minWidth: 2,
        minHeight: 2.5
    },
    {
        type: "living room",
        minLength: 4,
        minWidth: 2,
        minHeight: 2.5
    },
    {
        type: "kitchen",
        minLength: 3,
        minWidth: 1.5,
        minHeight: 2.5
    },
    {
        type: "storage",
        minLength: 1,
        minWidth: 1,
        minHeight: 2
    },
    {
        type: "utility",
        minLength: 1,
        minWidth: 2,
        minHeight: 2.3
    }
]

export default roomtype