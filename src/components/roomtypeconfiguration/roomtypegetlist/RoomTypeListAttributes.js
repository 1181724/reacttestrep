import React, {Component} from 'react';
import {connect} from "react-redux";
import {fetchRoomsType} from "../../../actions/actionsRoomType";

class RoomTypeListAttributes extends Component {

    render() {
        const {row} = this.props;
        return (
            <div>
                <p><strong>Name of Room Type: </strong>{row.type}</p>
                <p><strong>Height: </strong>{row.minHeight}</p>
                <p><strong>Width: </strong>{row.minWidth}</p>
                <p><strong>Length: </strong>{row.minLength}</p>
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        roomType: {
            loading: state.roomTypeReducer.roomType.loading,
            data: state.roomTypeReducer.roomType.data,
            error: state.roomTypeReducer.roomType.error,
        }
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        onFetchRoomsType: () => {
            dispatch(fetchRoomsType())
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(RoomTypeListAttributes);