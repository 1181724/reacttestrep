import React, {Component} from 'react';
import Collapsible from 'react-collapsible';
import RoomTypeListAttributes from "./RoomTypeListAttributes";

class ListRoomTypeCollapsibleButton extends Component {

    render() {
        const {row} = this.props;
        return (
            <Collapsible trigger={row.type}>
                <RoomTypeListAttributes row={row}/>
            </Collapsible>
        )
    }
}

export default ListRoomTypeCollapsibleButton;