import React, {Component} from 'react';
import connect from "react-redux/es/connect/connect";

import {createRoomType} from "../../../actions/actionsRoomType";

import {Button} from "reactstrap";

class CreateRoomTypeRoomType extends Component {

    constructor(props) {
        super(props);
        this.state = {type: '', minHeight: '', minWidth: '', minLength: ''};
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleInputChange = attribute => event => {
            this.setState({[attribute]: event.target.value});
            //this.handleSubmit = this.handleSubmit.bind(this);
        };
    }

    handleSubmit() {
        this.props.onAddRoomType(this.state);
        this.props.history.push('/house-configuration')
    }

    render() {
        return (
            <div>
                <label> Name: <input value={this.state.type} type="text" name="type"
                                    onChange={this.handleInputChange('type')}/> </label>

                <label> Height: <input value={this.state.minHeight} type="number" name="minHeight"
                                       onChange={this.handleInputChange('minHeight')}/> </label>

                <label> Width: <input value={this.state.minWidth} type="number" name="minWidth"
                                      onChange={this.handleInputChange('minWidth')}/> </label>

                <label> Length: <input value={this.state.minLength} type="number" name="minLength"
                                       onChange={this.handleInputChange('minLength')}/> </label>
                <Button
                    onClick={this.handleSubmit}>Save new room type configuration
                </Button>
            </div>)
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        onAddRoomType: ({type, minHeight, minWidth, minLength}) => {
            dispatch(createRoomType({type, minHeight, minWidth, minLength}))
        }

    }
};

export default connect(null, mapDispatchToProps)(CreateRoomTypeRoomType)