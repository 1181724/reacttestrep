import React, {Component} from 'react';
import {connect} from "react-redux";
import {fetchRoomsType} from "../../../actions/actionsRoomType";

class RoomTypeAttributes extends Component {

    constructor(props) {
        super(props);
        this.state = {
            roomType: ''
        };

        this.handleClick=this.handleClick.bind(this); // <--
    }

    /*
    constructor(props){
        super(props);
        this.state=
        {
            name:this.props.row.name
        }
        this.handleSubmit=this.handleSubmit.bind(this);
    }
     */

    handleClick = (type) => {
        this.props.onFetchRoomsType(type);
        this.setState({
            roomType: this.props.row.type,
        })
    }

    shouldComponentUpdate(nextProps, nextState) {
        console.log(nextProps)
        if (this.props.type !== nextProps.type) {
            return true;
        }

        return false;
    }

    render() {
        const {row} = this.props;
        return (
            <div>
                <p><strong>Name of Room: </strong>{row.type}</p>
                <p><strong>Height: </strong>{row.minHeight}</p>
                <p><strong>Width: </strong>{row.minWidth}</p>
                <p><strong>Length: </strong>{row.minLength}</p>



            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        roomType: {
            loading: state.roomTypeReducer.roomType.loading,
            data: state.roomTypeReducer.roomType.data,
            error: state.roomTypeReducer.roomType.error,
        }
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        onFetchRoomsType: (type) => {
            dispatch(fetchRoomsType(type)) // antes estava fetchRoomsType (que chama a lista)
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(RoomTypeAttributes);