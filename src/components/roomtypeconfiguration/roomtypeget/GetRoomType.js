import React, {Component} from 'react';
import connect from "react-redux/es/connect/connect";
import {fetchRoomType} from "../../../actions/actionsRoomType";
//import SubmitNameButton from "./SubmitNameButton";

class GetRoomType extends Component {

    /*constructor(props) {
        super(props);
        this.state = {roomTypeName: ''};
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleInputChange = this.handleInputChange.bind(this);

    }*/

    constructor(props) {
        super(props);
        this.state = {
            /*  Initial State */
            roomTypeName: "",
            getText: ""
        };
    }

    /*
    handleInputChange = input => {
        this.setState({ roomTypeName: input });
    };

    handleSubmit = roomTypeName =>
        {
            this.props.onFetchRoomType(roomTypeName)
        }
    */

    ////////
    /* handleChange() function to set a new state for input */
    handleChange = event => {
        const value = event.target.value;
        this.setState({
            roomTypeName: value
        });
    };
    ////////
    ////////
    /* handleGet() function to get the input and set to onFetchRoomType */
    handleGet = event => {
        event.preventDefault();
        const text = this.state.roomTypeName;
        this.setState({
            getText:
                this.props.onFetchRoomType(text)
        });
    };
    ////////

    render() {
        // <button onClick={this.handleSubmit}>Search</button>
        const {data} = this.props.roomType;

        console.log('log: ' + data.type)

        /*
        <input type="text" value={this.state.roomTypeName} name="roomTypeName"
                           onChange={(event) => this.handleInputChange(event.target.value)}
                    />
                    <SubmitNameButton onClick={this.handleSubmit}>Search</SubmitNameButton>
         */

        return(
            <div>
            <form onSubmit={this.handleGet}>
                <div>


                    <input
                        type="text"
                        value={this.state.roomTypeName}
                        onChange={this.handleChange}
                        placeholder="Enter a text"
                    />
                    <button>Get Type</button>
                </div>
            </form>

                <h5>Height: {{data}.minHeight}</h5>
                <h5>Width: {data.minWidth}</h5>
                <h5>Length: {data.minLength}</h5>

            </div>
        )


    }
}

const mapStateToProps = (state) => {
    return {
        roomType: {
            loading: state.roomTypeReducer.roomType.loading,
            data: state.roomTypeReducer.roomType.data,
            error: state.roomTypeReducer.roomType.error,
        }
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        onFetchRoomType: (roomTypeName) => {
            dispatch(fetchRoomType(roomTypeName))
            console.log("fetchRoomType: " + roomTypeName)
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(GetRoomType)