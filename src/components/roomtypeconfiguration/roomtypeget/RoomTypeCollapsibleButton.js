import React, {Component} from 'react';
import Collapsible from 'react-collapsible';
import RoomTypeAttributes from "./RoomTypeAttributes";

class RoomTypeCollapsibleButton extends Component {

    render() {
        const {row} = this.props;
        return (
            <Collapsible trigger={row.type}>
                <RoomTypeAttributes row={row}/>
            </Collapsible>
        )
    }
}

export default RoomTypeCollapsibleButton;