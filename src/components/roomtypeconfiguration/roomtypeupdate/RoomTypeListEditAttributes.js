import React, {Component} from 'react';
import {connect} from "react-redux";
import {editRoomType} from "../../../actions/actionsRoomType";

class RoomTypeListEditAttributes extends Component {

    constructor(props) {
        super(props);
        this.state = {
            isHidden: false,
            type: this.props.row.type,
            minHeight: 0,
            minWidth: 0,
            minLength: 0,
        };

        this.handleSubmit = this.handleSubmit.bind(this);

        this.handleInputChange = attribute => event => {
            this.setState({[attribute]: event.target.value});
        };
    }

    handleSubmit() {
        this.setState({
            isHidden:true
        })
        this.props.onEditRoomType(this.state)
    }

    render() {
        const {row} = this.props;
        return (
            <div>
                <p><strong>Name of Room: </strong>{row.type}</p>

                <p><strong>Height: </strong>{row.minHeight}</p>
                <input value={this.state.minHeight} type="text" name="minHeight"
                       onChange={this.handleInputChange('minHeight')}/>

                <p><strong>width: </strong>{row.minWidth}</p>
                <input value={this.state.minWidth} type="text" name="minWidth"
                       onChange={this.handleInputChange('minWidth')}/>

                       <p><strong>length: </strong>{row.minLength}</p>
                <input value={this.state.minLength} type="text" name="minLength"
                       onChange={this.handleInputChange('minLength')}/>

                       <button onClick={this.handleSubmit }> Edit</button>
            </div>
        )
    }
}


const mapDispatchToProps = (dispatch) => {
    return {
        onEditRoomType: ({type, minHeight, minWidth, minLength}) => {
            dispatch(editRoomType({type, minHeight, minWidth, minLength}))
        }
    }
};

export default connect(null, mapDispatchToProps)(RoomTypeListEditAttributes);