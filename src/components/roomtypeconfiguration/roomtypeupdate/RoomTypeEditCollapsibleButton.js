import React, {Component} from 'react';
import Collapsible from 'react-collapsible';
import RoomTypeListEditAttributes from "./RoomTypeListEditAttributes";

class RoomTypeEditCollapsibleButton extends Component {

    render() {
        const {row} = this.props;
        return (
            <Collapsible trigger={row.type}>
                <RoomTypeListEditAttributes row={row}/>
            </Collapsible>
        )
    }
}

export default RoomTypeEditCollapsibleButton;