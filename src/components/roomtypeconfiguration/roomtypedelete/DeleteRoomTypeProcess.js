import React, {Component} from 'react';
import {deleteRoomsType} from "../../../actions/actionsRoomType";
import connect from "react-redux/es/connect/connect";

//import {browserHistory} from "react-router";

class DeleteRoomTypeProcess extends Component {

    constructor(props) {
        super(props);
        this.state = {
            remove: false,
        }
    }

    handleClick = (e) => {
        window.confirm("Are you sure you wish to delete this room type?")
        this.props.onDeleteRoomsType(e.target.value);
        //e.preventDefault();
        //this.setState(true)
        //this.props.history.push('/house-configuration')
        //browserHistory.push('/house-configuration')
    }

    render() {

        const {row} = this.props;
        return (
            <div>
                <p>Name of Room Type: <strong>{row.type}</strong></p>
                <p>Height: <strong>{row.minHeight}</strong></p>
                <p>Width: <strong>{row.minWidth}</strong></p>
                <p>Length: <strong>{row.minLength}</strong></p>

                <button onClick={this.handleClick} value={row.type}>Delete</button>

                <div id="inf">e</div>

            </div>
        )
    }
}



const mapDispatchToProps = (dispatch) => {
    return {
        onDeleteRoomsType: (type) => {
            console.log("console:"+type)
            dispatch(deleteRoomsType(type))
        }
    }
}

export default connect(null, mapDispatchToProps) (DeleteRoomTypeProcess);