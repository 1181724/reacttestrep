import React, {Component} from 'react';
import DeleteRoomTypeButton from "./DeleteRoomTypeButton";
import connect from "react-redux/es/connect/connect";
import {fetchRoomsType} from "../../../actions/actionsRoomType";

class DeleteRoomType extends Component {

    componentDidMount() {
        this.props.onFetchRoomsType();
    }

    render() {
        const {loading, error, data} = this.props.roomType;

        if(loading){
            return (<h1>Loading ....</h1>);
        }
        if(error){
            return (<h1>Error ....</h1>);
        }
        if (data.length > 0) {
            const rows = data.map((row, index) => {
                return (
                    <tr key={index}>
                        <td>
                            <DeleteRoomTypeButton row={row}/>
                        </td>
                    </tr>
                )
            })
            return (
                <table>
                    <tbody>{rows}</tbody>
                </table>
            );
        } else {
            return (<h1>No data ....</h1>);
        }
    }
}

const mapStateToProps = (state) => {
    return {
        roomType: {
            loading: state.roomTypeReducer.roomType.loading,
            data: state.roomTypeReducer.roomType.data,
            error: state.roomTypeReducer.roomType.error,
        }
    }
}
const mapDispatchToProps = (dispatch) => {
    return {
        onFetchRoomsType: () => {
            dispatch(fetchRoomsType())
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(DeleteRoomType)