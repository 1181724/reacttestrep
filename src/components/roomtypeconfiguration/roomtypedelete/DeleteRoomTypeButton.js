import React, {Component} from 'react';
import Collapsible from 'react-collapsible';
import DeleteRoomTypeProcess from "./DeleteRoomTypeProcess";

class DeleteRoomTypeButton extends Component {

    render() {
        const {row} = this.props;

        return (

            <Collapsible trigger={row.type}>
                <DeleteRoomTypeProcess row={row}/>
            </Collapsible>
        )
    }
}

export default DeleteRoomTypeButton;