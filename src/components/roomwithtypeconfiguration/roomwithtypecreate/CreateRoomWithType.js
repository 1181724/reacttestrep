import React, {Component} from 'react';
import connect from "react-redux/es/connect/connect";

import {createRoomWithType} from "../../../actions/actionsRoomsWithType";

import {Button} from "reactstrap";

class CreateRoomWithType extends Component {

    constructor(props) {
        super(props);
        this.state = {name: '', roomType:{ type:'' }, description:'', houseFloor: '', height: '', width: '', length: ''};
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleInputChange = attribute => event => {
            this.setState({[attribute]: event.target.value});
        };
    }

    handleSubmit() {
        this.props.onCreateRoomWithType(this.state);
        this.props.history.push('/house-configuration')
    }

    render() {
        return (
            <div>
                    <label> Name: <input value={this.state.name} type="text" name="name"
                                        onChange={this.handleInputChange('name')}/> </label>

                    <label> RoomType: <input value={this.state.roomType.type} type="text" name="roomType"
                                     onChange={this.handleInputChange('roomType')}/> </label>

                    <label> Description: <input value={this.state.description} type="text" name="description"
                                                onChange={this.handleInputChange('description')}/> </label>

                    <label> HouseFloor: <input value={this.state.houseFloor} type="number" name="houseFloor"
                                               onChange={this.handleInputChange('houseFloor')}/> </label>

                    <label> Height: <input value={this.state.height} type="number" name="height"
                                           onChange={this.handleInputChange('height')}/> </label>

                    <label> Width: <input value={this.state.width} type="number" name="width"
                                          onChange={this.handleInputChange('width')}/> </label>

                    <label> Length: <input value={this.state.length} type="number" name="length"
                                           onChange={this.handleInputChange('length')}/> </label>

                    <Button
                        onClick={this.handleSubmit}>Save new room
                        configuration</Button>
            </div>
        )
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        onCreateRoomWithType: ({name, roomTypeName, description, houseFloor, height, width, length}) => {
            dispatch(createRoomWithType({name, roomTypeName, description, houseFloor, height, width, length}))
        }

    }
};

export default connect(null, mapDispatchToProps)(CreateRoomWithType)