import React, {Component} from 'react';
import Collapsible from 'react-collapsible';
import RoomWithTypeListAttributes from "./RoomWithTypeListAttributes";

class ListRoomWithTypeCollapsibleButton extends Component {

    render() {
        const {row} = this.props;
        return (
            <Collapsible trigger={row.name}>
                <RoomWithTypeListAttributes row={row}/>
            </Collapsible>
        )
    }
}

export default ListRoomWithTypeCollapsibleButton;