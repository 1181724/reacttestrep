import React, {Component} from 'react';
import {connect} from "react-redux";
import {fetchRoomsWithType} from "../../../actions/actionsRoomsWithType";

class RoomWithTypeListAttributes extends Component {

    render() {
        const {row} = this.props;

        // <p><strong>Name of Room Type: </strong>{row.roomType}</p>

        return (
            <div>
                <p><strong>Name of Room: </strong>{row.name}</p>
                <p><strong>Name of Room Type: </strong>{row.roomType.type}</p>
                <p><strong>Description: </strong>{row.description}</p>
                <p><strong>HouseFloor: </strong>{row.houseFloor}</p>
                <p><strong>Height: </strong>{row.height}</p>
                <p><strong>Width: </strong>{row.width}</p>
                <p><strong>Length: </strong>{row.length}</p>
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        roomWithType: {
            loading: state.roomWithTypeReducer.roomWithType.loading,
            data: state.roomWithTypeReducer.roomWithType.data,
            error: state.roomWithTypeReducer.roomWithType.error,
        }
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        onFetchRoomsWithType: () => {
            dispatch(fetchRoomsWithType())
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(RoomWithTypeListAttributes);