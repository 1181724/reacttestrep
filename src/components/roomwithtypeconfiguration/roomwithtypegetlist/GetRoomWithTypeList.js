import React, {Component} from 'react';
import connect from "react-redux/es/connect/connect";
import {fetchRoomsWithType} from "../../../actions/actionsRoomsWithType"

import ListRoomWithTypeCollapsibleButton from "./ListRoomWithTypeCollapsibleButton";

class GetRoomWithTypeList extends Component {

    /*constructor(props) {
        super(props);
        this.state = {roomsWithType: ''}
    }*/

    componentDidMount() {
        this.props.onFetchRoomsWithType();
    }

    render() {
        const {loading, error, data} = this.props.roomWithType;
        if (loading === true) {
            return (<h1>Loading ....</h1>);
        } else {
            if (error !== null) {
                return (<h1>Error ....</h1>);
            } else {
                if (data.length > 0) {
                    const rows = data.map((row, index) => {
                        return (
                            <tr key={index}>
                                <td>
                                    <ListRoomWithTypeCollapsibleButton row={row}/>
                                </td>
                            </tr>
                        )
                    })
                    return (
                        <table>
                            <tbody>{rows}</tbody>
                        </table>
                    );
                } else {
                    return (<h1>No data ....</h1>);
                }
            }
        }
    }
}

const mapStateToProps = (state) => {
    return {
        roomWithType: {
            loading: state.roomWithTypeReducer.roomWithType.loading,
            data: state.roomWithTypeReducer.roomWithType.data,
            error: state.roomWithTypeReducer.roomWithType.error,
        }
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        onFetchRoomsWithType: () => {
            dispatch(fetchRoomsWithType())
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(GetRoomWithTypeList)
